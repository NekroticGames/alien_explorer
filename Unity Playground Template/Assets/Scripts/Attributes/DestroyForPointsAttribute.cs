﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Playground/Attributes/Destroy For Points")]
public class DestroyForPointsAttribute : MonoBehaviour
{
	public int pointsWorth = 1;

	private UIScript userInterface;

	private void Start()
	{
		// Find the UI in the scene and store a reference for later use
		userInterface = GameObject.FindObjectOfType<UIScript>();
	}
	

	//This will create a dialog window asking for which dialog to add
	private void Reset()
	{
		Utils.Collider2DDialogWindow(this.gameObject, false);
	}
	
	//duplication of the following function to accomodate both trigger and non-trigger Colliders
	private void OnCollisionEnter2D(Collision2D collisionData)
	{
		OnTriggerEnter2D(collisionData.collider);
	}

	// This function gets called everytime this object collides with another trigger
	private void OnTriggerEnter2D(Collider2D collisionData)
	{
		// is the other object a Bullet?
		if(collisionData.gameObject.CompareTag("Bullet"))
	
        {
            // Assume the enemy died to one hit KO
			Debug.Log("BulletTouchEnemy");
			
            bool enemyDead = true;

            // Does this object have a HealthSystemAttribute?
            HealthSystemAttribute health = GetComponent<HealthSystemAttribute>();
            // If so, only consider the enemy dead if its health is 0
            if (health != null)
            {
                enemyDead = health.health == 0;
            }

            if (enemyDead)
            {
                // add one point
                BulletAttribute b = collisionData.gameObject.GetComponent<BulletAttribute>();
                if (b != null)
                {
                    // GrantPoints(b.playerId);

                    // Destroy the bullet if needed
                    if (b.bulletPierce == false)
                    {
                       // Destroy(collisionData.gameObject);
                    }
                }
                else
                {
                    Debug.Log("Use a BulletAttribute on one of the objects involved in the collision if you want one of the players to receive points for destroying the target.");
                }

                // then destroy this object
                Destroy(gameObject);
            }

		}
	}

    public void GrantPoints(int playerID)
    {
        if (userInterface != null)
        {
            userInterface.AddPoints(playerID, pointsWorth);
        }
        else
        {
            Debug.Log("There is no UI in the scene, hence points can't be displayed.");
        }

    }
}
