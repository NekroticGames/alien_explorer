﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[AddComponentMenu("Playground/Conditions/Condition Destroy")]
public class ConditionDestroy : ConditionBase
{
	
    // This function will be called by Unity when the object is destroyed
    void OnDestroy()
    {
        ExecuteAllActions(gameObject);
    }

}
