﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Playground/Conditions/Condition Timer")]
public class ConditionTimer : ConditionBase
{
	public float initialDelay = 0f;
    public bool repeat = true;
    public float frequency = 1f;

    private float timeLastEventFired;


	private void Start()
	{
		timeLastEventFired = initialDelay - frequency;
	}


	private void Update()
	{
		if(Time.time >= timeLastEventFired + frequency)
		{
			ExecuteAllActions(null);
			timeLastEventFired = Time.time;
            if (!repeat)
                enabled = false;
		}
	}
}