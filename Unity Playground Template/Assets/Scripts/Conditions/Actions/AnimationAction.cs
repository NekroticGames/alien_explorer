﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu("Playground/Actions/Animation")]
public class AnimationAction : Action
{
    public Animator objectToAnimate;

    public string parameterName;

    public AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Trigger;

    [ShowOnEnum("parameterType", (int)AnimatorControllerParameterType.Int)]
    public int parameterValueInt;

    [ShowOnEnum("parameterType", (int)AnimatorControllerParameterType.Float)]
    public float parameterValueFloat;

    [ShowOnEnum("parameterType", (int)AnimatorControllerParameterType.Bool)]
    public bool parameterValueBool;
    
    public void Start()
    {
        // If no specific object was requested to be animated, assume it either on the current object
        if (objectToAnimate == null)
        {
            objectToAnimate = GetComponent<Animator>();
        }

        if (objectToAnimate == null)
        {
            Debug.LogError("Failed to animate - no animator provided and no animator found on object " + name);
        }
    }

    public override bool ExecuteAction(GameObject dataObject)
    {
        if (objectToAnimate != null)
        {
            switch(parameterType)
            {
                case AnimatorControllerParameterType.Trigger:
                    objectToAnimate.SetTrigger(parameterName);
                    break;
                case AnimatorControllerParameterType.Int:
                    objectToAnimate.SetInteger(parameterName, parameterValueInt);
                    break;
                case AnimatorControllerParameterType.Float:
                    objectToAnimate.SetFloat(parameterName, parameterValueFloat);
                    break;
                case AnimatorControllerParameterType.Bool:
                    objectToAnimate.SetBool(parameterName, parameterValueBool);
                    break;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    
}
