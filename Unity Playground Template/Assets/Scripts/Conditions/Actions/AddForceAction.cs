﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[AddComponentMenu("Playground/Actions/Add Force")]
public class AddForceAction : Action
{
    [Header("AddForceAction")]
    public float forceStrength = 0;


    public enum ForceActsOn
    {
        THIS_OBJECT, TRIGGERING_OBJECT
    }

    [Tooltip("What object should the force acton?")]
    public ForceActsOn forceActsOn;


    public enum ForceDirectionMode
    {
        SPECIFIED, RELATIVE_TO_ORIENTATION, RELATIVE_TO_VELOCITY, RELATIVE_TO_TRIGGER
    }

    [Tooltip("What direction to the force is applied in")]
    public ForceDirectionMode forceDirectionMode;

    [ShowOnEnum("forceDirectionMode",(int)ForceDirectionMode.SPECIFIED)]
    public Vector2 forceDirection = new Vector2(1f, 1f);



    public override bool ExecuteAction(GameObject dataObject)
    {
        GameObject targetObject = gameObject;
        if (forceActsOn == ForceActsOn.TRIGGERING_OBJECT)
        {
            targetObject = dataObject;
        }

        Rigidbody2D targetBody = targetObject.GetComponent<Rigidbody2D>();

        if (targetBody == null)
        {
            return false;
        }

        Vector2 actualForceDirection = forceDirection;
        if (forceDirectionMode == ForceDirectionMode.RELATIVE_TO_ORIENTATION)
        {
            actualForceDirection = (Vector2)(Quaternion.Euler(0, 0, transform.eulerAngles.z) * forceDirection);
        }
        else if (forceDirectionMode == ForceDirectionMode.RELATIVE_TO_VELOCITY)
        {
            actualForceDirection = targetBody.velocity;
        }
        else if (forceDirectionMode == ForceDirectionMode.RELATIVE_TO_TRIGGER)
        {
            GameObject nonTarget = dataObject;
            if (forceActsOn == ForceActsOn.TRIGGERING_OBJECT)
            {
                nonTarget = gameObject;
            }
            actualForceDirection = (nonTarget.transform.position - targetObject.transform.position);
        }

        // Add a force to the object
        targetBody.AddForce(actualForceDirection.normalized * forceStrength);

        return true;
    }



}
